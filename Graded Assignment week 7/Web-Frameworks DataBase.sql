create database vijay;

use vijay;

create table book(id int(2) primary key,title varchar(20),author varchar(20),year int(4));
insert into book values(1,'A commentry','Apoorva',1981);
insert into book values(2,'cheque boo','vasdev mohi',1980);
insert into book values(3,'celestial bodies','jokha alharthi',1990);
insert into book values(4,'the overstory','Richard',1970);

select * from book;


create table favtable(id int(2) primary key,title varchar(20),author varchar(20),year int(4));
insert into favtable values(2,'cheque boo','vasdev mohi',1980);
insert into favtable values(3,'celestial bodies','jokha alharthi',1990);

select * from favtable;


create table bookscustomer(email varchar(30),name varchar(30),phone varchar(20),password varchar(20));

insert into bookscustomer values('vijay@gmail.com','vijay','9234719833','vijay');

insert into bookscustomer values('vijayvijay@gmail.com','bhaskar','9234719855','bhaskar');

insert into bookscustomer values('vijayreddy@gmail.com','reddy','9234719443','reddy');

select * from bookscustomer;

create table loginuser(email varchar(30),password varchar(20));

insert into loginuser values('vijay@gmail.com','vijay');
insert into loginuser values('vijayvijay@gmail.com','bhaskar');

select * from loginuser;
