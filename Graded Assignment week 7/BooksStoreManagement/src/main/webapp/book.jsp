<%@page import="com.bean.Book"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


    <h2>G vijay bhaskar reddy</h2>
	<%
	HttpSession sess = request.getSession();
	String email = (String) sess.getAttribute("email");
	if (email != null) {
	%>
	<h1>
		Welcome<%=email%></h1>
	<h3>
		<a href='books'>List Of Books</a>
	</h3>
	<h3>
		<a href='dashboard'>Dashboard</a>
	</h3>
	<h3>
		<a href='logout'>Logout</a>
	</h3>
	<h1>Available Books</h1>
	<%
	}
	%>
	<table border="1">
		<tr>
			<th>id</th>
			<th>title</th>
			<th>author</th>
			<th>year</th>
		</tr>
		<%
		List<Book> books = (List<Book>) request.getAttribute("list");
		for (Book book : books) {
		%>
		<tr>
			<td><p>
					id:<%=book.getId()%></p></td>
			<td><p>
					title:<%=book.getTitle()%></p></td>
			<td><p>
					author:<%=book.getAuthor()%></p></td>
			<td><p>
					year:<%=book.getYear()%></p></td>
			<td><a href="books?id=<%=book.getId()%>">Add to favr</a></td>

		</tr>
		<%
		}
		%>
	</table>
	<h3>
		<button type='button' onClick="location.href='favservlet'">View
			fav Books</button>

	</h3>
</body>
</html>