package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bean.BooksCustomer;
import com.resource.DBConnection;

public class BookCustomerDatabase {
	private static Connection con=DBConnection.getConnection();

	public List<BooksCustomer> getAllCustomers() throws SQLException
	{
		// to display books
		List<BooksCustomer> customers=new ArrayList<BooksCustomer>();
		String sql="select email,name,phone,password from bookscustomer";
		Statement statement =con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		while(rs.next())
		{

			BooksCustomer customer = new BooksCustomer();
			customer.setEmail(rs.getString(1));
			customer.setName(rs.getString(2));
			customer.setPhone(rs.getString(3));		
			customer.setPassword(rs.getString(4));
			customers.add(customer);
		}

		return customers;	
	}
	// to insert books
	public boolean insertCustomer(BooksCustomer customer)throws SQLException{
		String prepstsql="insert into bookscustomer(email,name,phone,password)values(?,?,?,?)";
		PreparedStatement ps=con.prepareStatement(prepstsql);

		ps.setString(1,customer.getEmail());
		ps.setString(2,customer.getName());		
		ps.setString(3,customer.getPhone());	
		ps.setString(4,customer.getPassword());
		ps.executeUpdate();
		return true;
	}
}
