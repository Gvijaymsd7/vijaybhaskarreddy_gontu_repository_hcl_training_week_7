package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bean.LoginUser;
import com.dao.LoginUserDatabase;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		System.out.println(request.getMethod());
		System.out.println(request.getContextPath());
		String email = request.getParameter("email");
		String pwd = request.getParameter("password");
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		out.println("<html><head><style>h1{color:red;}</style></head><body>");
		LoginUserDatabase loginUserDatabase = new LoginUserDatabase();
		LoginUser user = new LoginUser(email, pwd);
		if(loginUserDatabase.validate(user)) {
			HttpSession session=request.getSession();
			session.setAttribute("email", email);
			response.sendRedirect("dashboard");
		}
		else {
			response.sendRedirect("login.jsp");
		}

		out.println("</body></html>");

	}

}
