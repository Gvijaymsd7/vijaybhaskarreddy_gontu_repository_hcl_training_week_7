package com.controller;

import static com.utility.Constants.EMAIL;
import static com.utility.Constants.NAME;
import static com.utility.Constants.PASSWORD;
import static com.utility.Constants.PHONE;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.BooksCustomer;
import com.dao.BookCustomerDatabase;

/**
 * Servlet implementation class Register
 */
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.sendRedirect("register.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String email=request.getParameter(EMAIL);
		String name=request.getParameter(NAME);
		String phone=request.getParameter(PHONE);
		String password=request.getParameter(PASSWORD);

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		if( (!!email.isEmpty() || email != null) && 
				(name != null && !name.trim().isEmpty()) &&
				(!phone.isEmpty() && phone != null) &&
				(!password.isEmpty() && password != null))

		{
			System.out.println("if not null");
			BooksCustomer c1 = new BooksCustomer(email,name,phone,password);
			System.out.println(c1);
			BookCustomerDatabase database=new BookCustomerDatabase();
			try {
				if(database.insertCustomer(c1))
				{
					System.out.println("insert true");
					response.sendRedirect("login.jsp");
				}
				else {
					System.out.println("insert false");
					RequestDispatcher rd = request.getRequestDispatcher("register.jsp");

					out.println("Please contact administrator");
					rd.include(request, response);
				}
			} catch (SQLException | ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
			out.println("Fill in all details");
			RequestDispatcher rd = request.getRequestDispatcher("register.jsp");			
			rd.include(request, response);
			//response.sendRedirect("register.html");
		}
	}
	}


